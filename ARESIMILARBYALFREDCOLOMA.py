from contracts import contract
"""
areSimilar

Two arrays are called similar if one can be obtained from another by swapping at most one pair of elements in one of the arrays.
Given two arrays a and b, check whether they are similar.

Example

For a = [1, 2, 3] and b = [1, 2, 3], the output should be
areSimilar(a, b) = true.
The arrays are equal, no need to swap any elements.

For a = [1, 2, 3] and b = [2, 1, 3], the output should be
areSimilar(a, b) = true.
We can obtain b from a by swapping 2 and 1 in b.

For a = [1, 2, 2] and b = [2, 1, 1], the output should be
areSimilar(a, b) = false.
Any swap of any two elements either in a or in b won't make a and b equal.

[Contract] Input/Output

[input] array.integer a : Array of integers.

Guaranteed constraints:
3 ≤ a.length ≤ 105,
1 ≤ a[i] ≤ 1000.

[input] array.integer b : Array of integers of the same length as a.

Guaranteed constraints:
b.length = a.length,
1 ≤ b[i] ≤ 1000.

[output] boolean

true if a and b are similar, false otherwise.

"""

@contract
def areSimilar(a:"list[N](int,>=1,<=1000),N <=105,N >=3", b:"list[H](int,>=1,<=1000),H==N") -> bool:
    n = sum([ 1 for i, j in zip(a,b) if i != j ])
    a.sort()
    b.sort()
    if n<=2 and a == b:
        return True
    return False

#a = [1, 2, 3] ; b = [1, 2, 3]
#a = [1, 2, 3] ; b = [2, 1, 3]
#a = [1, 2, 2] ; b = [2, 1, 1]
a = [1, 1, 4] ; b = [1, 2, 3,1]
#a = [1, 2, 4] ; b = [1, 2, 3, 4]
#a = [100, 200, 400, 999] ; b = [100, 200, 999, 400]
#a = [-1, 2, 3] ; b = [-1, 2, 3]
#a = list(range(200)) ; b = list(range(198)) + [199, 198]
#a = [1000, 2000, 3000] ; b = [1000, 3000, 2000]

print(areSimilar(a,b))